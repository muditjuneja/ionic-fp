import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private fingerprint: FingerprintAIO) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  async ngOnInit() {
    try {
      const isAvailable = await this.fingerprint.isAvailable();
      alert(isAvailable);
      if (isAvailable == 'OK') {
        alert('Yayy! FP available');
        let response = await this.fingerprint.show({ clientId: 'mudit', clientSecret: 'hello', disableBackup: false });
        alert(JSON.stringify(response));
        console.log(response);
      } else {
        alert('FP not available');
      }
    } catch (error) {
      console.log(error);
    }
  }
}

